﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Drive.v3;
using Google.Apis.Drive.v3.Data;
using Google.Apis.Services;
using Google.Apis.Util.Store;
using System.IO;
using System.Threading;

namespace GoogleDriver
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            Control.CheckForIllegalCrossThreadCalls = false;
        }

        static string[] Scopes = { DriveService.Scope.Drive };/* Cấu hình phạm vi thao tác dữ liệu với scope là Drive, Tức là
        cho phép người dùng xem, thêm mới, sửa các file trên google Drive*/
        static string ApplicationName = "Application_upload_file";

        
        private  void ListFiles(DriveService service, ref string pageToken)// Tạo request để đưa các file lên google drive
        {
           
            FilesResource.ListRequest listRequest = service.Files.List();
            // Define parameters (thông số) of request.

            listRequest.PageSize = 1;//  trả về trên một trang
            //listRequest.Fields = "nextPageToken, files(id, name)";
            listRequest.Fields = "nextPageToken, files(name)";
            listRequest.PageToken = pageToken;
            listRequest.Q = "mimeType='image/*'";

            // List files.
            var request = listRequest.Execute();


            if (request.Files != null && request.Files.Count > 0)//nếu số lượng file chọn lớn hơn 0 
            {
                foreach (var file in request.Files)
                {
                    textBox1.Text += string.Format("{0}", file.Name); // in ra tên file ở chổ tình trạng 
                }

                pageToken = request.NextPageToken;

                if (request.NextPageToken != null)
                {
                    Console.ReadLine();
                }
            } else 
            {
                textBox1.Text +=("No files found.");
            }
        }

       

        private  void Upload(string path, DriveService service, string folderUpload)
        {
            // khởi tạo biến 
            var fileMetadata = new Google.Apis.Drive.v3.Data.File();
            fileMetadata.Name = Path.GetFileName(path);
            fileMetadata.MimeType = "image/*";
            // Định nghĩa file gửi là ảnh hay các file khác
            fileMetadata.Parents = new List<string>
            {
                folderUpload
            };

            FilesResource.CreateMediaUpload request;
            using (var stream = new System.IO.FileStream(path, System.IO.FileMode.Open))
            {
                // Tạo file trong folder
                request = service.Files.Create(fileMetadata, stream, "image/*");
                // Cấu hình thông tin lấy về là "id"
                request.Fields = "id";
                // Thực hiện upload
                request.Upload();
            }
            var file = request.ResponseBody;
            //textBox1.Text += ("File ID: " + file.Id);

        }


        // Xác thực tài khoản đăng nhập vào Google Drive của người dùng lần đầu tiên
        // Sau khi đăng nhập thông tin sẽ được lưu vào "file.json mà mình đã lấy"
        // lần đầu chạy chương trình, sẽ lấy thông tin đăng nhập lại từ đây và lần sau ko cần nữa
        private  UserCredential GetCredentials()
        {
            UserCredential credential;
            using (var stream = new FileStream("client_secret.json", FileMode.Open, FileAccess.Read))
            {
                string credPath = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);

                credPath = Path.Combine(credPath, "client_secret.json");

                // Yêu cầu người dùng xác thực cho lần đầu và thông tin lưu vào file.json
                credential = GoogleWebAuthorizationBroker.AuthorizeAsync(
                    GoogleClientSecrets.Load(stream).Secrets,
                    Scopes,// quyền truy xuất dữ liệu của người dùng
                    "user",
                    CancellationToken.None,
                    new FileDataStore(credPath, true)).Result;
                 textBox1.Text = string.Format("Credential file saved to: " + credPath);
            }
            return credential;
        }

     

        private void btnBrowse_Click(object sender, EventArgs e)
        {
            // lấy giấy phép xác thực Google Drive.
            UserCredential credential;
            credential = GetCredentials();
            // Tạo dịch vụ Drive API
            var service = new DriveService(new BaseClientService.Initializer()
            {
                HttpClientInitializer = credential,
                ApplicationName = ApplicationName,
            });
           
            string folderid;
            // Lấy folder bằng tên
            var fileMetadatas = new Google.Apis.Drive.v3.Data.File()//khai báo một đối tượng folder nhờ thuộc tính MineType = "application/vnd.google-apps.folder"
            {
                Name = txtFolderNameUpload.Text,
                MimeType = "application/vnd.google-apps.folder"
            };
            var requests = service.Files.Create(fileMetadatas);//thực hiện việc tạo folder trên gg drive bằng phương thức create
            requests.Fields = "id";//nhận về ID của folder đã tạo ra do gg drive cung cấp
            var files = requests.Execute();
            folderid = files.Id;
           
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                txtFileSelected.Text = "Đang upload file đến folder: => " + txtFolderNameUpload.Text + "\r\n\r\n";
                foreach (string filename in openFileDialog1.FileNames)
                {
                    Thread thread = new Thread(() =>
                    {
                        Upload(filename, service, folderid);
                        txtFileSelected.Text += filename + " => upload thành công..." + "\r\n";
                    });
                    thread.IsBackground = true;
                    thread.Start(); 
                }              
            }

            string pageToken = null;
            do {
                ListFiles(service, ref pageToken);
            } while (pageToken != null);

            textBox1.Text += "Upload file thành công.";
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void txtFolderNameUpload_TextChanged(object sender, EventArgs e)
        {

        }

  
    } 
}
